/*
 * @Author: lihaogn
 * @Date: 2021-06-02 23:27:15
 * @LastEditTime: 2022-09-04 11:38:07
 * @LastEditor: lihaogn
 * @Description: vue webpack 配置
 * @FilePath: \lix-admin-client-vue2\vue.config.js
 */
const path = require('path');

module.exports = {
  // 开发环境配置
  devServer: {
    port: 8585,
    proxy: {
      '/api': {
        target: 'http://127.0.0.1:7001/', // 你请求的第三方接口，在 /api 前面加上路径
        changeOrigin: true // 在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题

        // pathRewrite: { // 路径重写，
        //     '^/api': '' // 替换target中的请求地址，也就是说以后你在请求http://127.0.01:7001/这个地址的时候直接写成/api即可。
        // }
      },
      '/socket.io': {
        target: 'http://127.0.0.1:7001/', // 你请求的第三方接口
        ws: true,
        changeOrigin: true // 在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题
      }
    }
  },

  /**
   * 生产环境配置
   */

  // 部署应用包时的基本 URL，默认值：'/'
  // "/" ==> https://www.my-app.com/
  // "/app" ==> https://www.my-app.com/app/
  // publicPath: process.env.NODE_ENV === 'production' ? '/' : '/',

  // 生产环境构建文件的目录，默认 'dist'；目标目录在构建之前会被清除
  // outputDir:'dist',

  // 生产环境不使用sourceMap，加速构建，默认为 true
  productionSourceMap: process.env.NODE_ENV === 'production' ? false : true,

  // css 配置
  // css: {
  // 是否将组件中的 CSS 提取至一个独立的 CSS 文件中
  // 默认值：生产环境下是 true，开发环境下是 false
  //   extract: true
  // }

  // 对内部的 webpack 配置进行更细粒度的修改
  // chainWebpack: config => {
  //     // 发布模式
  //     config.when(process.env.NODE_ENV === 'production', config => {
  //         //entry找到默认的打包入口，调用clear则是删除默认的打包入口
  //         //add添加新的打包入口
  //         config.entry('app').clear().add('./src/main-prod.js')
  //         //使用externals设置排除项
  //         config.set('externals', {
  //             'vue': 'Vue',
  //             'vue-router': 'VueRouter',
  //             'axios': 'axios',
  //             'element-ui': 'ELEMENT',
  //         })
  //         // 使用插件，定制首页
  //         config.plugin('html').tap(args => {
  //             // 添加参数 isProd
  //             args[0].isProd = true
  //             return args
  //         })
  //     })
  //     // 开发模式
  //     config.when(process.env.NODE_ENV === 'development', config => {
  //         config.entry('app').clear().add('./src/main-dev.js')
  //         // 使用插件，定制首页
  //         config.plugin('html').tap(args => {
  //             // 添加参数 isProd
  //             args[0].isProd = false
  //             return args
  //         })
  //     })
  // }

  // 通用配置
  pluginOptions: {
    i18n: {
      locale: 'zh',
      fallbackLocale: 'zh',
      localeDir: 'locales',
      enableInSFC: true,
      enableBridge: false
    },
    'style-resources-loader': {
      preProcessor: 'less',
      patterns: [path.resolve(__dirname, './src/assets/style/global.less')],
    },
  }
}
