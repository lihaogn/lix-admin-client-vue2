# Lix-Admin-Client-Vue2

![1640931485613](./README.assets/1640931485613.png)

## 1 安装与运行

```
node version: 14.17.6
npm version: 8.1.3
```

### 1.1 开发环境

将项目下载或克隆到本地，进入目录中，在终端中输入命令：

```shell
# 1 安装依赖
npm install
# 2 运行程序
npm run dev
```

注意：需要配合后端项目 [**Lix-admin-server**](https://gitee.com/lihaogn/lix-admin-server)  一起使用。

### 1.2 生产环境

**1 将项目下载或克隆到本地，进入目录中，在终端中输入命令：**

```shell
# 1 安装依赖包
npm install
# 2 编译，编译后生成 dist 文件夹
npm run build
```

**2 将 dist 文件夹上传到服务器中**

**3 配置 Nginx**

创建文件 /etc/nginx/conf.d/lix-admin-vue2.conf :

```
server {
	listen       8585;
	server_name  localhost; 

	# 指向 dist 所在的目录
	root /home/lihao/app/lix-admin-vue2/dist; 

	location / {
		try_files $uri $uri/ @router;
		index  index.html index.htm;
	}

	location @router{
		rewrite ^.*$/index.html last;
	}

	location /api{
		proxy_pass http://localhost:7001/api;
	}

	location /socket.io{
		proxy_pass http://localhost:7001/socket.io;
		# 保持长连接
		proxy_http_version 1.1;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
	}

	error_page   500 502 503 504  /50x.html;
	location = /50x.html {
		root html;
	}
}
```

**4 运行 Nginx**

```shell
nginx -c /etc/nginx/nginx.conf
```

**问题：**

1 socket 可能出现跨域问题：The request client is not a secure context and the resource is in more-private address space...

解决：设置 Chrome，地址栏输入 chrome://flags/#block-insecure-private-network-requests，设置为 disabled。

2 如果访问网页是 403，那么可能是 root 配置的路径没有权限访问。

3 如果操作接口返回的是502，那么可能是后端服务没有启动好。

**5 Jenkins shell 配置（使用 Jenkins 的情况）**

vue 的配置：

```shell
npm install
npm run build
rm -rf /home/lix-admin/vue/dist
cp -r dist /home/lix-admin/vue/
```

server 的配置：

```shell
BUILD_ID=dontKillMe # 防止把进程杀掉
cp -R ./* /home/lix-admin/serve
cd /home/lix-admin/serve
npm install
npm run stop
nohup npm run start &
```

## 2 现有功能

- 登录与登出
  - 访问权限：根据不同的用户，展示不同的菜单内容。
- 用户管理
  - 新增、修改用户信息
  - 删除账号
  - 设置账号状态（启用、停用、注销）
  - 导入、导出用户信息
- 角色管理
  - 新增、修改、删除角色
  - 为角色分配权限
- 权限管理
  - 新增、修改、删除权限
  - 页面访问权限与操作权限
- 全屏
- 搜索菜单
- 更换主题色
- 选择语言（翻译定义不全）

## 开发日志

**2022.05.31**

- [x] 新增国际化分支 i18n，补充国际化内容。

**2022.05.29**

- [x] 超级管理员不做按钮权限限制。

**2022.05.28**

- [x] 添加了新增子权限后父级自动展开的功能。

**2022.05.27**

- [x] 后端修改了用户与角色的关系，前端做相应的修改。
- [x] 页签刷新的方法更新。

```vue
// 先注册一个名为 `redirect` 的路由
<script>
export default {
  beforeCreate() {
    const { params, query } = this.$route
    const { path } = params
    this.$router.replace({ path: '/' + path, query })
  },
  render: function(h) {
    return h() // avoid warning message
  }
}
</script>
```

```javascript
// 手动重定向页面到 '/redirect' 页面
const { fullPath } = this.$route
this.$router.replace({
  path: '/redirect' + fullPath
})
```

```javascript
// 加一个路由
{
    path: '/redirect',
    component: Home,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
```

**2022.05.26**

关于权限与角色：

- [x] 超级管理员拥有所有权限。
- [x] 其他角色不能修改超级管理员的权限。

**2022.01.06**

- [x] 文件的上传与下载功能

**2021.12.28**

- [x] 记录 npm、node 版本
- [x] 测试一下系统功能，修改bug

**2021.12.27**

- [x] 使用 Jenkins 部署项目。

**2021.12.25**

- [x] import 后直接 export 写法。

**2021.12.23**

- [x] 超级管理员不能删除 -> 初始用户不能删除。

**2021.12.21**

- [x] 角色、权限新增/修改点击确定后未校验表单。
- [x] 添加404 页面。

**2021.12.18**

- [x] 导入用户的时候不给角色和是否启用的信息了吧，默认是普通用户和启用状态。

**2021.12.16**

- [x] drawer, dialog 还是不要封装了，太麻烦了，有很多种情况。。。

