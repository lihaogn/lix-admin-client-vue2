/*
 * @Author: lihaogn
 * @Date: 2022-09-04 10:32:06
 * @LastEditTime: 2022-09-04 11:10:03
 * @LastEditor: lihaogn
 * @Description: highlight 组件（高亮代码）
 * @FilePath: \lix-fim-client-vue2\src\components\common\lix-highlight\index.js
 */
import hljs from 'highlight.js/lib/common';

function escapeHTML(value) {
  return value
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#x27;');
}

const Component = {
  props: {
    // 代码语言
    language: {
      type: String,
    },
    // 代码内容
    code: {
      type: String,
      required: true,
    },
    // 是否自动探测代码语言，优先级高
    autodetect: {
      type: Boolean,
    },
  },
  data() {
    return {
      detectedLanguage: '',
      unknownLanguage: false,
    };
  },
  computed: {
    className() {
      if (this.unknownLanguage) return '';

      return 'hljs ' + this.detectedLanguage;
    },
    highlighted() {
      // 没有匹配的代码语言时，使用原始的代码，不经过高亮处理
      if (!this.autoDetect && !hljs.getLanguage(this.language)) {
        console.warn(
          `The language "${this.language}" you specified could not be found.`
        );
        this.unknownLanguage = true;
        return escapeHTML(this.code);
      }

      let result = {};
      if (this.autoDetect) {
        result = hljs.highlightAuto(this.code);
        this.detectedLanguage = result.language;
      } else {
        result = hljs.highlight(this.code, {
          language: this.language,
          ignoreIllegals: true,
        });
        this.detectedLanguage = this.language;
      }
      return result.value;
    },
    autoDetect() {
      return !this.language || this.autodetect;
    },
  },

  render(createElement) {
    return createElement('pre', {}, [
      createElement('code', {
        class: this.className,
        domProps: { innerHTML: this.highlighted },
      }),
    ]);
  },
  // template: `<pre><code :class="className" v-html="highlighted"></code></pre>`
};

export default {
  install(Vue) {
    Vue.component('highlightjs', Component);
  },
  component: Component,
};
