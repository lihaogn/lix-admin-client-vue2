/*
 * @Author: lihaogn
 * @Date: 2021-12-08 00:04:20
 * @LastEditTime: 2021-12-18 21:48:52
 * @LastEditor: lihaogn
 * @Description: 导入导出辅助函数
 * @FilePath: \lix-admin-vue2\src\components\common\lix-import\utils.js
 */

import XLSX from 'xlsx'
/**
 * 获取表头（通用方式）
 */
export const getHeaderRow = sheet => {
  const headers = []
  const range = XLSX.utils.decode_range(sheet['!ref'])
  let C
  const R = range.s.r
  /* start in the first row */
  for (C = range.s.c; C <= range.e.c; ++C) {
    /* walk every column in the range */
    const cell = sheet[XLSX.utils.encode_cell({ c: C, r: R })]
    /* find the cell in the first row */
    let hdr = 'UNKNOWN ' + C // <-- replace with your desired default
    if (cell && cell.t) hdr = XLSX.utils.format_cell(cell)
    headers.push(hdr)
  }
  return headers
}

/**
 * 导入数据对应表头
 */
export const USER_RELATIONS = {
  用户名: 'userName',
  真实姓名: 'userRealname',
  手机号码: 'userMobile',
  邮箱: 'userEmail'
}
