/*
 * @Author: lihaogn
 * @Date: 2021-12-08 17:12:51
 * @LastEditTime: 2021-12-12 18:40:30
 * @LastEditor: lihaogn
 * @Description: 导出工具方法
 * @FilePath: \lix-admin-vue2\src\components\common\lix-export\utils.js
 */

/**
 * 导出数据对应表头
 */
export const USER_RELATIONS = {
  用户名: 'userName',
  真实姓名: 'userRealname',
  手机号码: 'userMobile',
  邮箱: 'userEmail',
  创建时间: 'createdAt',
  修改时间: 'updatedAt'
}
