/*
 * @Author: lihaogn
 * @Date: 2021-05-15 19:10:14
 * @LastEditTime: 2022-05-29 10:42:19
 * @LastEditor: lihaogn
 * @Description:
 * @FilePath: \lix-admin-client-vue2\src\plugins\element.js
 */
import Vue from 'vue'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(Element)
