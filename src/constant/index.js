/*
 * @Author: lihaogn
 * @Date: 2021-11-14 10:36:55
 * @LastEditTime: 2022-09-04 11:20:48
 * @LastEditor: lihaogn
 * @Description: 常量
 * @FilePath: \lix-admin-client-vue2\src\constant\index.js
 */
export const TOKEN = 'token'
export const USER_ID = 'userId'
export const USER_NAME = 'userName'
export const MAIN_COLOR = 'mainColor'
export const DEFAULT_MAIN_COLOR = '#409EFF'
export const TAGS_VIEW = 'tagsView'
// 工作模式
export const WORK_MODE = 'workMode'
// 侧边栏打开状态
export const SIDEBAR_OPENED = 'sidebarOpened'
// 用户状态枚举对象
export const USER_STATUS = {
  ON: 1,
  OFF: 2,
  DEAD: 3
}
// 权限类型
export const PERMISSION_TYPE = {
  PAGE: 1,
  ACTION: 2
}