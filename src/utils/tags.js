/*
 * @Author: lihaogn
 * @Date: 2021-11-27 16:07:57
 * @LastEditTime: 2022-05-28 16:21:44
 * @LastEditor: lihaogn
 * @Description: 标签的工具函数
 * @FilePath: \lix-admin-client-vue2\src\utils\tags.js
 */

// 非标签页的 path 集合
const whiteList = ['/login', '/redirect']

/**
 * @author: lihaogn
 * @Date: 2021-11-27 16:08:55
 * @description: 判断路径的页面是否是标签页
 * @param {*} path - 页面路径
 * @return {*}
 */
export function isTags(path) {
  return !whiteList.includes(path) && !/\/redirect\/.*/.test(path)
}
