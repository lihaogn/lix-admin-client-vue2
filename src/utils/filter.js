/*
 * @Author: lihaogn
 * @Date: 2021-12-07 16:31:52
 * @LastEditTime: 2021-12-26 14:47:12
 * @LastEditor: lihaogn
 * @Description: 过滤器
 * @FilePath: \lix-admin-vue2\src\utils\filter.js
 */
export {
  formatDate as dateFilter,
  relativeTime as relativeTimeFilter
} from './common'
