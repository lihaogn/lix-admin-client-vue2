/*
 * @Author: lihaogn
 * @Date: 2021-11-14 10:23:38
 * @LastEditTime: 2021-11-14 10:27:14
 * @LastEditor: lihaogn
 * @Description: 本地存储工具
 * @FilePath: \lix-admin-vue2\src\utils\storage.js
 */

/**
 * 存储数据
 */
export const lsSetItem = (key, value) => {
  // 将数组、对象类型的数据转化为 JSON 字符串进行存储
  if (typeof value === 'object') {
    value = JSON.stringify(value)
  }
  window.localStorage.setItem(key, value)
}

/**
 * 获取数据
 */
export const lsGetItem = key => {
  const data = window.localStorage.getItem(key)
  try {
    return JSON.parse(data)
  } catch (err) {
    return data
  }
}

/**
 * 删除数据
 */
export const lsRemoveItem = key => {
  window.localStorage.removeItem(key)
}

/**
 * 删除所有数据
 */
export const lsRemoveAllItem = key => {
  window.localStorage.clear()
}
