/*
 * @Author: lihaogn
 * @Date: 2021-11-23 00:45:03
 * @LastEditTime: 2022-08-13 11:23:22
 * @LastEditor: lihaogn
 * @Description: 转换颜色工具
 * @FilePath: \lix-fim-client-vue2\src\utils\theme.js
 */
import color from 'css-color-function';
import rgbHex from 'rgb-hex';
import formula from '@/constant/formula.json';
import { getRequest } from '@/utils/request.js';

/**
 * 写入样式到 <style> 中
 * @param {*} elNewStyle:string - element 新样式
 */
const writeNewStyle = elNewStyle => {
  const style = document.createElement('style');
  style.innerText = elNewStyle;
  document.head.appendChild(style);
};

/**
 * 根据主色生成色值表
 * @param {*} primary:String - 主颜色
 * @returns colors:object - 色值表（格式类似 formula）
 */
const generateColors = primary => {
  if (!primary) return;
  const colors = {
    primary,
    // successColor
  };
  Object.keys(formula).forEach(key => {
    const value = formula[key].replace(/primary/g, primary);
    colors[key] = '#' + rgbHex(color.convert(value));
  });

  // {primary:'#xxx', 'shade-1':'#xxx', 'light-1':'#xxx', ...}
  return colors;
};

/**
 * 获取当前 element-ui 的默认样式表（把色值转换成标记）
 * @returns *:string - 原样式模板
 */
const getOriginalStyle = async () => {
  // const version = require('element-ui/package.json').version
  // 访问远程的样式文件
  // const url = `https://unpkg.com/element-ui@${version}/lib/theme-chalk/index.css`

  // 访问本地的样式文件，文件存放在 /public/api/ 下
  const url = `/css/element-theme.css`;
  const cssStr = await getRequest(url);
  // 把获取到的数据筛选为原样式模板
  return getStyleTemplate(cssStr);
};

/**
 * 返回 style 的 template
 * 把原样式表中的色值转换成标记，用于替换新颜色
 * @param {*} data:string - 原 element-ui index.css 文本字符串
 * @returns data:string - 转换后的样式模板（色值变成了标记）
 */
const getStyleTemplate = data => {
  // element-ui 默认色值，value 和 formula.json 中的 key 一致
  const colorMap = {
    '#3a8ee6': 'shade-1',
    '#409eff': 'primary',
    '#53a8ff': 'light-1',
    '#66b1ff': 'light-2',
    '#79bbff': 'light-3',
    '#8cc5ff': 'light-4',
    '#a0cfff': 'light-5',
    '#b3d8ff': 'light-6',
    '#c6e2ff': 'light-7',
    '#d9ecff': 'light-8',
    '#ecf5ff': 'light-9',
  };
  // 根据默认色值为要替换的色值打上标记
  Object.keys(colorMap).forEach(key => {
    const value = colorMap[key];
    data = data.replace(new RegExp(key, 'ig'), value);
  });
  return data;
};

/**
 * 根据主色值，生成最新的样式表
 * @param {*} primaryColor:String - 主颜色
 * @returns cssText:string - 生成的新样式表（带标记的）
 */
export const makeNewStyle = async primaryColor => {
  // 1 根据主色生成色值表
  const colors = generateColors(primaryColor);
  // 2 获取 element-ui 的原样式模板
  let cssText = await getOriginalStyle();

  // 3 遍历生成的样式表，在 CSS 的原样式中进行全局替换
  Object.keys(colors).forEach(key => {
    cssText = cssText.replace(
      new RegExp('(:|\\s+)' + key, 'g'),
      '$1' + colors[key]
    );
  });

  // 4 将样式写入到页面中
  // return cssText
  writeNewStyle(cssText);
};
