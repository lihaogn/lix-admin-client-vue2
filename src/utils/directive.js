import store from '../store'

/*
 * @Author: lihaogn
 * @Date: 2021-12-23 00:09:31
 * @LastEditTime: 2022-05-29 10:58:26
 * @LastEditor: lihaogn
 * @Description: 自定义指令
 * @FilePath: \lix-admin-client-vue2\src\utils\directive.js
 */

/**
 * @author: lihaogn
 * @Date: 2021-12-23 00:23:49
 * @description: 操作权限
 * @param {*}
 * @return {*}
 */
const permission = (el, binding) => {
  const { value } = binding
  const actionPermissions = store.getters.userInfo.permission.actions
  if (value && value instanceof Array) {
    // 匹配对应的指令
    const hasPermission = actionPermissions.some(action =>
      value.includes(action.mark)
    )
    // 如果无法匹配，则表示当前用户无该指令，那么删除对应的功能按钮
    if (
      !hasPermission &&
      !store.getters.userInfo.role.some(r => r.id === '001')
    ) {
      el.parentNode && el.parentNode.removeChild(el)
    }
  } else {
    throw new Error('v-permission value is ["xxx","xxx"]')
  }
}

export { permission }
