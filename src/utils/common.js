/*
 * @Author: lihaogn
 * @Date: 2021-08-29 13:30:29
 * @LastEditTime: 2022-08-11 23:32:44
 * @LastEditor: lihaogn
 * @Description: 通用工具函数
 * @FilePath: \lix-fim-client-vue2\src\utils\common.js
 */

/**
 * @author: lihaogn
 * @Date: 2021-12-23 10:38:49
 * @description: 生成id
 * @return {*} 4位数字
 */
const generateId = function() {
  return Math.floor(Math.random() * 10000);
};

import dayjs from 'dayjs';
import 'dayjs/locale/zh-cn'; // 语言包
import rt from 'dayjs/plugin/relativeTime'; // 处理相对时间
dayjs.extend(rt);
/**
 * @author: lihaogn
 * @Date: 2021-12-23 10:39:40
 * @description: 格式化日期
 * @param {String|Object} val 日期字符串
 * @param {String} format 格式化字符串
 * @return {*}
 */
const formatDate = (val, format = 'YYYY-MM-DD HH:mm') => {
  return dayjs(val).format(format);
};

/**
 * @author: lihaogn
 * @Date: 2021-12-26 14:17:19
 * @description: 获取相对时间
 * @param {*} val:String - 日期字符串
 * @return {*}
 */
const relativeTime = val => {
  if (!isNaN(val)) {
    val = parseInt(val);
  }
  return dayjs()
    .locale('zh-cn')
    .to(dayjs(val));
};

/**
 * @description: 数字转换成字符串
 * @param {Number|String} num 数值
 * @param {Number} precision 精度
 * @return {*}
 */
const numberToString = (num, precision = 2) => {
  let numValue = num;

  return typeof numValue === 'string' ? numValue : numValue.toFixed(precision);
};

export { generateId, formatDate, relativeTime, numberToString };
