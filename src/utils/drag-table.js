/*
 * @Author: lihaogn
 * @Date: 2021-12-26 17:17:38
 * @LastEditTime: 2021-12-26 17:26:28
 * @LastEditor: lihaogn
 * @Description: 拖拽表格
 * @FilePath: \lix-admin-vue2\src\utils\drag-table.js
 */

import Sortable from 'sortablejs'

/**
 * 初始化排序
 */
export const initSortable = (ref, tableData, cb) => {
  // 设置拖拽效果
  const el = ref.$el.querySelectorAll(
    '.el-table__body-wrapper > table > tbody'
  )[0]
  // 1. 要拖拽的元素
  // 2. 配置对象
  Sortable.create(el, {
    // 拖拽时类名
    ghostClass: '',
    // 拖拽结束的回调方法
    onEnd(event) {
      const { newIndex, oldIndex } = event
      console.log('newIndex', newIndex, 'oldIndex', oldIndex)
      cb && cb()
    }
  })
}
