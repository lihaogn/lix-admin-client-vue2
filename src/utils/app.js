/*
 * @Author: lihaogn
 * @Date: 2021-11-15 22:44:08
 * @LastEditTime: 2022-01-09 13:21:16
 * @LastEditor: lihaogn
 * @Description: app 工具函数
 * @FilePath: \lix-admin-base-vue2\src\utils\app.js
 */

/**
 * @author: lihaogn
 * @Date: 2021-11-27 10:53:16
 * @description: 生成侧边菜单对象
 * @param {*} routes:array - 路由表数组
 * @return {*}
 */
export function generateMenus(routes) {
  const result = []

  routes.forEach(item => {
    if (item.parent === undefined && item.meta.isMenu) {
      // 先生成父节点
      result.push(item)
    } else if (item.meta.isMenu) {
      if (item.parent && item.parent.name === 'root') {
        result.push(item)
        return true
      }

      const parent = routes.find(x => {
        return x.path === item.parent.path
      })
      // 添加子节点
      if (parent) {
        if (parent.children) {
          if (parent.children.some(x => x.path === item.path)) {
            return true
          }
          parent.children.push(item)
        } else {
          parent.children = [item]
        }
      }
    }
  })

  return result
}

/**
 * @author: lihaogn
 * @Date: 2021-11-27 10:54:20
 * @description: 生成可搜索的路由对象
 * @param {*} routes:array - 路由表数组
 * @param {*} prefixTitle:string - 前缀名称
 * @return {*}
 */
export function generateRoutesForSearch(routes, prefixTitle = []) {
  let res = []

  routes.forEach(item => {
    const data = {
      path: item.path,
      title: [...prefixTitle]
    }

    // 正则，匹配动态路由，动态路由过滤掉
    const re = /.*\/:.*/
    if (item.meta && item.meta.title && re.exec(item.path) == null) {
      if (item.meta.canSearched) {
        res.push(data)
      }
      data.title.push(item.meta.title)
    }

    // 子节点
    if (item.children) {
      const childRes = generateRoutesForSearch(item.children, data.title)
      if (childRes.length > 0) {
        res.push(...childRes)
      }
    }
  })

  return res
}
