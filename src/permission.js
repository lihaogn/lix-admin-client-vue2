/*
 * @Author: lihaogn
 * @Date: 2021-11-14 11:25:31
 * @LastEditTime: 2022-09-04 11:26:55
 * @LastEditor: lihaogn
 * @Description: 鉴权
 * @FilePath: \lix-admin-client-vue2\src\permission.js
 */

import router from '@/router';
import store from '@/store';

// 白名单
const whiteList = ['Login'];

// 全局前置守卫
router.beforeEach(async (to, from, next) => {
  if (store.getters.token) {
    // 如果有 token（登录信息）
    if (to.name === 'Login') {
      // 如果访问 login 页面，直接跳转
      next();
    } else {
      // 跳转到 login 之外的页面
      // 判断用户信息是否获取
      // 若不存在用户信息，则需要获取用户信息
      if (!store.getters.userInfo.id) {
        // 没有用户信息，则去获取用户信息的 action，如果获取失败则登出
        // const { id, permission } =
        await store.dispatch('user/getUserInfo');

        // 不加这句刷新后页面空白或404
        next({ ...to, replace: true });
        // next(to.path)
      } else {
        next();
      }
    }
  } else {
    // 没有 token（没有登录信息）
    if (whiteList.indexOf(to.name) > -1) {
      // 如果访问的是白名单，就直接跳转
      next();
    } else {
      next('/login');
    }
  }
});
