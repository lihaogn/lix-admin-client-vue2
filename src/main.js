/*
 * @Author: lihaogn
 * @Date: 2021-05-15 19:10:14
 * @LastEditTime: 2022-09-04 11:26:39
 * @LastEditors: Please set LastEditors
 * @Description: 主入口
 * @FilePath: \lix-admin-client-vue2\src\main.js
 */
import Vue from 'vue';
import VueSocketIO from 'vue-socket.io';
import md5 from 'md5';
import _ from 'lodash';

import highlightPlugin from "/src/components/common/lix-highlight";
import 'highlight.js/styles/androidstudio.css';

import '@/assets/style/my-iconfont/iconfont.css';
import './plugins/element.js';
import App from './App.vue';
import router from './router';
import store from './store';
import './permission.js';

Vue.config.productionTip = false;
Vue.prototype.$md5 = md5;
Vue.prototype.$_ = _;

// 高亮代码插件
Vue.use(highlightPlugin);

/**
 * socket 配置
 */
const vueSocketIO = new VueSocketIO({
  debug: true,
  connection: process.env.VUE_APP_SOCKET_URL,
});
// 监听connect事件
vueSocketIO.io.on('connect', () => {
  console.log('socket connect from main.js');
});
Vue.use(vueSocketIO);

/**
 * 全局过滤器
 */
import * as filters from './utils/filter';
Object.keys(filters).forEach(key => {
  // 插入过滤器名和对应方法
  Vue.filter(key, filters[key]);
});

/**
 * 全局自定义指令
 */
import * as directives from './utils/directive';
Object.keys(directives).forEach(key => {
  Vue.directive(key, directives[key]);
});

/**
 * 打印
 */
import Print from 'vue-print-nb';
Vue.use(Print);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
