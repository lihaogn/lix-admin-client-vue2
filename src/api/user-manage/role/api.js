/*
 * @Author: lihaogn
 * @Date: 2021-12-18 19:23:44
 * @LastEditTime: 2021-12-20 20:47:20
 * @LastEditor: lihaogn
 * @Description: 角色管理相关的接口
 * @FilePath: \lix-admin-vue2\src\api\user-manage\role\api.js
 */

import {
  getRequest,
  postRequest,
  putRequest,
  deleteRequest
} from '@/utils/request.js'

// ========== 请求路径 ==========
const roleReqPath = '/api/role'

// ========== 请求方法  ==========

// 获取角色列表
const reqGetRoleList = () => getRequest(roleReqPath)
// 新增角色
const reqNewRole = params => postRequest(roleReqPath, params)
// 修改角色
const reqUpdateRole = (key, params) =>
  putRequest(roleReqPath + '/' + key, params)
// 删除用户
const reqDeleteRole = (key, params) =>
  deleteRequest(roleReqPath + '/' + key, params)

export { reqGetRoleList, reqNewRole, reqUpdateRole, reqDeleteRole }
