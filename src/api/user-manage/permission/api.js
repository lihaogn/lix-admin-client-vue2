/*
 * @Author: lihaogn
 * @Date: 2021-12-18 21:27:08
 * @LastEditTime: 2021-12-20 00:56:22
 * @LastEditor: lihaogn
 * @Description:
 * @FilePath: \lix-admin-vue2\src\api\user-manage\permission\api.js
 */
import {
  getRequest,
  postRequest,
  putRequest,
  deleteRequest
} from '@/utils/request.js'

// ========== 请求路径 ==========
const permissionReqPath = '/api/permission'

// ========== 请求方法  ==========

// 获取权限列表
const reqGetPermissionList = () => getRequest(permissionReqPath)
// 新增权限
const reqNewPermission = params => postRequest(permissionReqPath, params)
// 修改权限
const reqUpdatePermission = (key, params) =>
  putRequest(permissionReqPath + '/' + key, params)
// 删除用户
const reqDeletePermission = (key, params) =>
  deleteRequest(permissionReqPath + '/' + key, params)

export {
  reqGetPermissionList,
  reqNewPermission,
  reqUpdatePermission,
  reqDeletePermission
}
