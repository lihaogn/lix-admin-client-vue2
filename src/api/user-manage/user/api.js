/*
 * @Author: lihaogn
 * @Date: 2021-06-04 22:10:45
 * @LastEditTime: 2022-01-08 16:30:57
 * @LastEditor: lihaogn
 * @Description:
 * @FilePath: \lix-admin-base-vue2\src\api\user-manage\user\api.js
 */

import {
  getRequest,
  postRequest,
  putRequest,
  deleteRequest
} from '@/utils/request.js'

// ========== 请求路径 ==========
const userReqPath = '/api/user'

// ========== 请求方法  ==========
// 获取单个用户信息
const reqGetUserInfo = key => getRequest(userReqPath + '/' + key)
// 获取用户列表
const reqGetUserList = () => getRequest(userReqPath)
// 分页获取用户列表
const reqGetUserListByPage = params => getRequest(userReqPath, params)
// 查询用户是否存在
const reqQueryOneUser = params => getRequest(userReqPath, params)

// 新增用户
const reqNewUser = params => postRequest(userReqPath, params)
// 导入用户
const reqImportUsers = params =>
  postRequest(userReqPath + '/' + 'import', params)

// 修改用户信息
const reqEditUser = (key, params) => putRequest(userReqPath + '/' + key, params)

// 删除用户
const reqDeleteUser = (key, params) =>
  deleteRequest(userReqPath + '/' + key, params)

export {
  reqGetUserList,
  reqEditUser,
  reqQueryOneUser,
  reqNewUser,
  reqDeleteUser,
  reqGetUserInfo,
  reqGetUserListByPage,
  reqImportUsers
}
