/*
 * @Author: lihaogn
 * @Date: 2021-05-30 09:22:14
 * @LastEditTime: 2022-09-04 11:19:09
 * @LastEditors: Please set LastEditors
 * @Description: 登录接口方法
 * @FilePath: \lix-admin-client-vue2\src\api\login\api.js
 */
import { getRequest, postRequest } from '@/utils/request.js';

// ========== 请求路径 ==========
const loginPath = '/api/login';
const logoutPath = '/api/logout';

// ========== 对应请求方法  ==========

// 登录
const reqLogin = params => postRequest(loginPath, params);
const reqLogout = params => getRequest(logoutPath, params);

export { reqLogin, reqLogout };
