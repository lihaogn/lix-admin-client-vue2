/*
 * @Author: lihaogn
 * @Date: 2021-11-14 11:27:18
 * @LastEditTime: 2022-08-13 20:43:17
 * @LastEditor: lihaogn
 * @Description: getters
 * @FilePath: \lix-fim-client-vue2\src\store\getters.js
 */
const getters = {
  token: state => state.user.token,
  userInfo: state => state.user.userInfo,
  sidebarOpened: state => state.app.sidebarOpened,
  cssVar: state => state.theme.variables,
  mainColor: state => state.theme.mainColor,
  tagsViewList: state => state.app.tagsViewList,
  workMode: state => state.app.workMode,
  excludeCachedViews: state => state.app.excludeCachedViews,
};

export default getters;
