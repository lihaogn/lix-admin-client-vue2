/*
 * @Author: lihaogn
 * @Date: 2021-06-24 19:38:08
 * @LastEditTime: 2021-12-21 21:41:21
 * @LastEditor: lihaogn
 * @Description: vuex
 * @FilePath: \lix-admin-vue2\src\store\index.js
 */

import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import getters from './getters'
import app from './modules/app'
import theme from './modules/theme'
import permission from './modules/permission'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  getters,
  modules: {
    user,
    app,
    theme,
    permission
  }
})
