/*
 * @Author: lihaogn
 * @Date: 2021-11-18 22:29:13
 * @LastEditTime: 2022-08-13 22:59:44
 * @LastEditor: lihaogn
 * @Description: app 模块
 * @FilePath: \lix-fim-client-vue2\src\store\modules\app.js
 */

import { lsGetItem, lsSetItem } from '@/utils/storage.js';
import { TAGS_VIEW, WORK_MODE, SIDEBAR_OPENED } from '@/constant';
export default {
  namespaced: true,
  state: () => ({
    // 侧边栏展开状态
    sidebarOpened: lsGetItem(SIDEBAR_OPENED) == false ? false : true,
    // 页签
    tagsViewList: lsGetItem(TAGS_VIEW) || [],
    // 不需要被缓存的页签
    excludeCachedViews: ['redirect-comp'],
    // 工作模式
    workMode: lsGetItem(WORK_MODE) == false ? false : true,
  }),
  mutations: {
    /**
     * @description: 添加不需要被缓存的页签
     * @param {Object} state
     * @param {Object} view 页签的路由信息
     * @return {*}
     */

    addExcludeCachedView(state, view) {
      const index = state.excludeCachedViews.indexOf(view.name);
      index === -1 && state.excludeCachedViews.push(view.name);
    },
    /**
     * @author: lihaogn
     * @Date: 2021-12-16 00:09:16
     * @description: 设置工作模式
     * @param {*} state
     * @param {Boolean} mode: - 工作模式值（true - 前端；false - 后端）
     * @return {*}
     */
    toggleWorkMode(state, mode) {
      state.workMode = mode;
      lsSetItem(WORK_MODE, mode);
    },
    /**
     * @author: lihaogn
     * @Date: 2021-11-21 23:06:44
     * @description: 切换侧边栏展开状态
     * @param {*} state
     * @return {*}
     */
    toggleSidebar(state) {
      state.sidebarOpened = !state.sidebarOpened;
      lsSetItem(SIDEBAR_OPENED, state.sidebarOpened);
    },

    /**
     * @author: lihaogn
     * @Date: 2021-11-27 16:39:55
     * @description: 添加一个 tagView
     * @param {Object} state
     * @param {Object} tag 页签路由信息
     * @return {*}
     */
    addTagsView(state, tag) {
      const isExist = state.tagsViewList.findIndex(
        item => item.path === tag.path
      );
      if (isExist === -1) {
        // 不存在则添加
        state.tagsViewList.push(tag);
        lsSetItem(TAGS_VIEW, state.tagsViewList);
      }

      const index = state.excludeCachedViews.indexOf(tag.name);
      index > -1 && state.excludeCachedViews.splice(index, 1);
    },
    /**
     * @author: lihaogn
     * @Date: 2021-11-27 19:45:09
     * @description: 删除一个 tagView
     * @param {*} state
     * @param {String} type - 关闭的类型
     * @param {Number} index - 当前点击的 tag 的 index
     * @return {*}
     */
    removeTagsView(state, { type, index }) {
      if (type === 'index') {
        // 关闭当前
        state.tagsViewList.splice(index, 1);
      } else if (type === 'other') {
        // 关闭其他
        state.tagsViewList.splice(index + 1);
        state.tagsViewList.splice(0, index);
      } else if (type === 'right') {
        // 关闭右侧
        state.tagsViewList.splice(index + 1);
      } else if (type == 'all') {
        // 关闭所有
        state.tagsViewList = [];
      }
      lsSetItem(TAGS_VIEW, state.tagsViewList);
    },
  },
  actions: {},
};
