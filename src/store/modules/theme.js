/*
 * @Author: lihaogn
 * @Date: 2021-11-21 22:40:54
 * @LastEditTime: 2021-12-23 10:22:29
 * @LastEditor: lihaogn
 * @Description: 主题模块
 * @FilePath: \lix-admin-vue2\src\store\modules\theme.js
 */
import { lsGetItem, lsSetItem } from '@/utils/storage.js'
import { MAIN_COLOR, DEFAULT_MAIN_COLOR } from '@/constant'
import variables from '@/assets/style/variables.less'
export default {
  namespaced: true,
  state: () => ({
    // 主题色
    mainColor: lsGetItem(MAIN_COLOR) || DEFAULT_MAIN_COLOR,
    // css 变量
    variables
  }),
  mutations: {
    /**
     * @author: lihaogn
     * @Date: 2021-11-21 22:44:49
     * @description: 设置主题色
     * @param {*} state
     * @param {*} color - 颜色值
     * @return {*}
     */
    setMainColor(state, color) {
      state.mainColor = color
      state.variables.menuBackgroundColor = color
      lsSetItem(MAIN_COLOR, color)
    }
  }
}
