/*
 * @Author: lihaogn
 * @Date: 2021-12-21 21:20:47
 * @LastEditTime: 2022-05-28 16:30:40
 * @LastEditor: lihaogn
 * @Description: 开发路由表
 * @FilePath: \lix-admin-client-vue2\src\router\modules\develop\index.js
 */
import Home from '@/views/layout/index.vue';

export default {
  name: 'Develop',
  path: '/develop',
  component: Home,
  meta: {
    title: '开发',
    iconName: 'el-icon-edit',
    canSearched: false,
    isMenu: true,
  },
  children: [
    {
      path: 'test',
      name: 'Test',
      component: () => import('../../../views/develop/Test.vue'),
      meta: {
        title: '测试',
        iconName: 'el-icon-odometer',
        canSearched: true,
        isMenu: true,
      },
    },
    {
      path: 'template',
      name: 'Template',
      component: () => import('../../../views/develop/index.vue'),
      meta: {
        title: 'Demo',
        iconName: 'el-icon-document-copy',
        canSearched: true,
        isMenu: true,
      },
    },
    {
      path: 'template/tableViewTemplateDemo',
      name: 'TableViewTemplateDemo',
      component: () =>
        import('../../../views/develop/template/LixTableViewTemplate.vue'),
      meta: {
        title: '表格模板',
        iconName: 'el-icon-menu',
        isMenu: false,
        canSearched: true,
      },
    },
  ],
};
