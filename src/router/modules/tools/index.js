/*
 * @Author: lihaogn
 * @Date: 2022-09-04 00:02:18
 * @LastEditTime: 2022-09-04 00:03:58
 * @LastEditor: lihaogn
 * @Description: 工具集合路由
 * @FilePath: \lix-fim-client-vue2\src\router\modules\tools\index.js
 */
import Home from '@/views/layout/index.vue';

export default {
  name: 'Tools',
  path: '/tools',
  component: Home,
  meta: {
    title: '工具',
    iconName: 'el-icon-edit',
    canSearched: false,
    isMenu: true,
  },
  children: [
    {
      path: 'highlight',
      name: 'Highlight',
      component: () => import('@/views/tools/HighlightCode.vue'),
      meta: {
        title: '高亮代码',
        iconName: 'el-icon-odometer',
        canSearched: true,
        isMenu: true,
      },
    },
   
  ],
};