/*
 * @Author: lihaogn
 * @Date: 2021-12-22 11:05:34
 * @LastEditTime: 2022-08-15 23:57:11
 * @LastEditor: lihaogn
 * @Description: 系统管理路由
 * @FilePath: \lix-fim-client-vue2\src\router\modules\system-manage\index.js
 */
import Home from '@/views/layout/index.vue';
export default {
  name: 'SystemManage',
  path: '/system-manage',
  component: Home,
  meta: {
    title: '系统管理',
    iconName: 'el-icon-setting',
    isMenu: true,
    canSearched: false,
  },
  children: [
    {
      path: 'user-manage',
      name: 'UserManage',
      component: () => import('@/views/system/user-manage/user-list/index.vue'),
      meta: {
        title: '用户管理',
        iconName: 'el-icon-user',
        isMenu: true,
        canSearched: true,
      },
    },
    {
      path: 'role-manage',
      name: 'RoleManage',
      component: () => import('@/views/system/user-manage/role-list/index.vue'),
      meta: {
        title: '角色管理',
        iconName: 'el-icon-s-custom',
        isMenu: true,
        canSearched: true,
      },
    },
    {
      path: 'permission-manage',
      name: 'PermissionManage',
      component: () =>
        import('@/views/system/user-manage/permission-list/index.vue'),
      meta: {
        title: '权限管理',
        iconName: 'el-icon-key',
        isMenu: true,
        canSearched: true,
      },
    },
  ],
};
