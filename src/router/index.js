/*
 * @Author: lihaogn
 * @Date: 2021-06-12 23:44:45
 * @LastEditTime: 2022-09-04 11:23:21
 * @LastEditors: Please set LastEditors
 * @Description: 路由配置
 * @FilePath: \lix-admin-client-vue2\src\router\index.js
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/common/Login.vue'
import Home from '@/views/layout/index.vue'

import developRoute from './modules/develop/index'
import systemManageRoute from './modules/system-manage/index'
import toolsRoute from './modules/tools/index'

Vue.use(VueRouter)

export const privateRoutes = [developRoute, systemManageRoute, toolsRoute]

export const publicRoutes = [
  {
    path: '/redirect',
    component: Home,
    children: [
      {
        name: 'Redirect',
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    name: 'Login',
    path: '/login',
    component: Login
  },
  {
    path: '/',
    redirect: '/welcome',
    component: Home,
    children: [
      {
        name: 'Welcome',
        path: 'welcome',
        component: () => import('@/views/common/Welcome.vue'),
        meta: {
          title: '首页',
          iconName: 'el-icon-s-home',
          isMenu: true
        }
      },
      {
        name: '404',
        path: '404',
        component: () => import('@/views/common/404.vue'),
        meta: {
          title: '404'
        }
      }
    ]
  }
]

const createRouter = () =>
  new VueRouter({
    // routes: [...publicRoutes, ...privateRoutes]
    routes: publicRoutes
  })

const router = createRouter()

/**
 * @author: lihaogn
 * @Date: 2021-12-22 09:29:24
 * @description: 重置路由
 * @param {*}
 * @return {*}
 */
export function resetRouter() {
  // 新路由实例matcer，赋值给旧路由实例的matcher，（相当于replaceRouter）
  router.matcher = createRouter().matcher
}

export default router
