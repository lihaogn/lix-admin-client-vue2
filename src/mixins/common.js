/*
 * @Author: lihaogn
 * @Date: 2021-12-05 20:21:40
 * @LastEditTime: 2022-09-04 11:21:15
 * @LastEditor: lihaogn
 * @Description: 通用的 mixins
 * @FilePath: \lix-admin-client-vue2\src\mixins\common.js
 */

const commonMixin = {
  data() {
    return {
      screenHeight: document.body.clientHeight
    }
  },

  watch: {
    screenHeight(val) {
      this.handleResize()
    }
  },
  mounted() {
    // 监听 resize 事件
    window.addEventListener(
      'resize',
      this.$_.debounce(event => {
        this.screenHeight = document.body.clientHeight
      }, 300)
    )
  },
  beforeDestroy() {
    // 销毁 resize 监听事件
    window.removeEventListener(
      'resize',
      this.$_.debounce(event => {
        this.screenHeight = document.body.clientHeight
      }, 300)
    )
  },
  methods: {
    /**
     * @author: lihaogn
     * @Date: 2021-12-12 17:03:46
     * @description: resize 事件
     * @param {*}
     * @return {*}
     */
    handleResize() {
      console.log('resize...')
    }
  }
}
export { commonMixin }
