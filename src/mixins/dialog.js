/*
 * @Author: lihaogn
 * @Date: 2021-12-08 14:32:25
 * @LastEditTime: 2021-12-19 16:39:45
 * @LastEditor: lihaogn
 * @Description: 对话框的 mixin
 * @FilePath: \undefinedd:\workspace\webProjects\VueProjects\lix-admin-vue2\src\mixins\dialog.js
 */
const dialogMixin = {
  props: {
    // 对话框标题
    title: {
      type: String,
      default: '对话框'
    },
    // 对话框显示与隐藏
    visible: {
      type: Boolean,
      required: true,
      default: false
    },
    /**
     * 对话框的类型（例如：新增/修改）
     *   新增：NEW
     *   修改：EDIT
     */
    type: {
      type: String
    },
    // 父组件传递的数据
    dialogData: {
      type: Array | Object
    }
  },
  data() {
    return {
      dialogVisible: false,
      dialogLoading: false,
      okButtonLoading: false,
      loading: false
    }
  },
  watch: {
    visible(val) {
      if (val) {
        this.dialogVisible = val
      }
    }
  }
}

export { dialogMixin }
