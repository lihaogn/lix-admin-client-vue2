/*
 * @Author: lihaogn
 * @Date: 2021-12-15 23:34:01
 * @LastEditTime: 2021-12-20 16:04:13
 * @LastEditor: lihaogn
 * @Description: 抽屉的 mixin
 * @FilePath: \lix-admin-vue2\src\mixins\drawer.js
 */
const drawerMixin = {
  props: {
    // 抽屉标题
    title: {
      type: String,
      default: '抽屉'
    },
    // 抽屉显示与隐藏
    visible: {
      type: Boolean,
      required: true,
      default: false
    },
    // 父组件传递的数据
    drawerData: {
      type: Array | Object
    }
  },
  data() {
    return {
      drawerVisible: false,
      loading: false
    }
  },
  watch: {
    visible(val) {
      if (val) {
        this.drawerVisible = val
      }
    }
  }
}

export { drawerMixin }
