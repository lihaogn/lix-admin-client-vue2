/*
 * @Author: lihaogn
 * @Date: 2022-05-26 20:46:32
 * @LastEditTime: 2022-09-04 11:24:30
 * @LastEditor: lihaogn 
 * @Description: prettier 配置
 * @FilePath: \lix-admin-client-vue2\.prettierrc.js
 */
module.exports = {
  semi: true, // 末尾加分号
  singleQuote: true, // 使用单引号
  trailingComma: 'es5', // 末尾加逗号
};