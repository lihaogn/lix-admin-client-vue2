/*
 * @Author: lihaogn
 * @Date: 2022-03-10 15:24:35
 * @LastEditTime: 2022-05-16 11:27:10
 * @LastEditor: lihaogn
 * @Description:
 * @FilePath: \lix-admin-vue2\docs\.vuepress\config.js
 */
module.exports = {
  title: 'Hello VuePress',
  description: 'Just playing around',
  // base: '/', // 部署站点的基础路径，base 将会作为前缀自动地插入到所有以 / 开始的其他选项的链接中，所以你只需要指定一次。默认 '/'
  port: 8888, // 指定 dev server 的端口，默认 8080
  // head: [
  //   // 注入到当前页面的 HTML <head> 中的标签
  //   ['link', { rel: 'icon', href: '/logo.jpg' }] // 增加一个自定义的 favicon(网页标签的图标)
  // ],

  markdown: {
    lineNumbers: false // 代码块显示行号
  },
  themeConfig: {
    nav: [
      // 导航栏配置
      { text: '指南', link: '/guide/' },
      { text: '博客', link: 'https://blog.csdn.net/lihaogn' },
      // 下拉列表
      {
        text: '仓库',
        items: [
          {
            text: 'Lix-Admin-Vue2',
            link: 'https://gitee.com/lihaogn/lix-admin-vue2'
          },
          {
            text: 'Lix-Admin-Server',
            link: 'https://gitee.com/lihaogn/lix-admin-server'
          }
        ]
      }
    ],
    // 侧边栏配置
    sidebar: {
      // '/': ['/'],
      '/guide/': ['/guide/']
    },
    sidebarDepth: 2 // 侧边栏显示2级
  }
}
